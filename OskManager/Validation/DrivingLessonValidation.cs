﻿using OskManager.Data;
using OskManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Validation
{
    public class DrivingLessonValidation
    {
        readonly ApplicationDbContext _context;

        public DrivingLessonValidation(ApplicationDbContext context)
        {
            _context = context;
        }

        public static bool IsOverlapping(DrivingLesson drivingLesson, ApplicationDbContext _context)
        {
            
            var overlaping = _context.DrivingLessons.Where(x => x.BeginningDate < drivingLesson.EndingDate && drivingLesson.BeginningDate 
            < x.EndingDate && x.Instructor == drivingLesson.Instructor);
            if (overlaping.Count() > 0)
            {
                return true;
            }
            else
                return false;
        }

        public static bool MatchesSettings(DrivingLesson drivingLesson, ApplicationDbContext _context)
        {
            var settings = _context.ReservationSettings.FirstOrDefault();
            var drivingLessonsThisDay = _context.DrivingLessons
                .Where(x => x.BeginningDate.Date == drivingLesson.BeginningDate.Date).Count();
            var difference = drivingLesson.EndingDate - drivingLesson.BeginningDate;
            var minutes = (int) difference.TotalMinutes;

            if (minutes < settings.MinTime || minutes > settings.MaxTime || drivingLessonsThisDay > settings.ReservationsPerDay)
            {
                return false;
            }
            else if (drivingLesson.BeginningDate.Date != drivingLesson.EndingDate.Date)
            {
                return false;
            }

            return true;
            
        }

        public static bool HasEnoughAvailableTime(Student student, int LessonMinutes)
        {
            if (student.AvailableMinutes >= LessonMinutes)
                return true;
            else
                return false;
                    
        }

        public static int GetMinutes(DateTime startTime, DateTime endTime)
        {
            TimeSpan varTime = endTime - startTime;
            double fractionalMinutes = varTime.TotalMinutes;
            int wholeMinutes = (int)fractionalMinutes;

            return wholeMinutes;
        }


    }
}
