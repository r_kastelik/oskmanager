﻿using Microsoft.EntityFrameworkCore;
using OskManager.Data;
using OskManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Validation
{
    public class WorkingHoursValidation
    {
        public static bool ValidateLicence(WorkingHours workingHour, ApplicationDbContext _context)
        {
            var instructor = _context.Instructors.Where(x => x.Id == workingHour.Instructor.Id).Include(y=>y.InstructorLicence).FirstOrDefault();
            var instructorLicence = _context.InstructorLicences.Where(x => x.LicenceNumber == instructor.InstructorLicence.LicenceNumber).FirstOrDefault();
            
            if (DateTime.Compare(instructorLicence.ValidThru,workingHour.EndTime)<0)
            {
                return false;
            }
            return true;
        }

    }
}
