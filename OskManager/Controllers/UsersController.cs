﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OskManager.Data;
using OskManager.Models;
using OskManager.BLL;
using Microsoft.EntityFrameworkCore;
using OskManager.Models.AccountViewModels;
using Microsoft.AspNetCore.Authorization;

namespace OskManager.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IUserService _userService;

        public UsersController(ApplicationDbContext context)
        {
            _context = context;
            _userService = new UserService(_context);
        }

        public async Task<IActionResult> Students()
        {
            return View(await _userService.GetAllStudentsAsync());
        }

        public async Task<IActionResult> EditStudent(string id)
        {
            var student = await _context.Students.Where(x => x.Id == id).Include(y => y.Address).FirstOrDefaultAsync();
            if (student == null)
            {
                return NotFound();
            }
            else
            {
                RegisterStudentViewModel toEdit = new RegisterStudentViewModel()
                {
                    FirstName = student.FirstName,
                    LastName = student.LastName,
                    Pesel = student.Pesel,
                    Street = student.Address.Street,
                    City = student.Address.City,
                    PostalCode = student.Address.PostalCode,
                    PhoneNumber = student.PhoneNumber,
                    Email = student.Email,
                    Pkk = student.Pkk
                };

                return View(toEdit);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditStudent(string id, RegisterStudentViewModel toEdit)
        {
            if (ModelState.IsValid)
            {
                if (!(_context.Users.Where(x => x.Email == toEdit.Email)).Any(y => y.Id != id))
                {
                    await _userService.UpdateStudentAsync(id, toEdit);
                    return RedirectToAction(nameof(Students));
                }
                ModelState.AddModelError(string.Empty, "Istnieje już użytkownik o takim adresie email.");
            }
            return View(toEdit);
        }

        public async Task<IActionResult> DeleteStudent(string id)
        {
            var student = await _userService.GetStudentByIdAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        [HttpPost, ActionName("DeleteStudent")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _userService.RemoveStudentAsync(id);
            return RedirectToAction(nameof(Students));
        }

        public async Task<IActionResult> StudentDetails(string id)
        {
            return View(await _userService.GetStudentByIdAsync(id));
        }

        public async Task<IActionResult> Instructors()
        {
            return View(await _userService.GetAllInstructorsAsync());
        }

        public async Task<IActionResult> EditInstructor(string id)
        {
            var categories = _context.Categories.ToList();
            var instructor = await _userService.GetInstructorByIdAsync(id);
            if (instructor == null)
            {
                return NotFound();
            }
            else
            {
                var userLicence = _context.LicenceCategories.Where(x => x.InstructorLicenceId == instructor.InstructorLicence.LicenceId).Select(y => y.CategoryId).ToList();

                foreach (var item in categories)
                {
                    if (userLicence.Contains(item.CategoryId))
                    {
                        item.IsChecked = true;
                    }
                    else item.IsChecked = false;
                }

                ViewBag.Categories = categories;

                RegisterInstructorViewModel toEdit = new RegisterInstructorViewModel()
                {
                    FirstName = instructor.FirstName,
                    LastName = instructor.LastName,
                    Pesel = instructor.Pesel,
                    Street = instructor.Address.Street,
                    City = instructor.Address.City,
                    PostalCode = instructor.Address.PostalCode,
                    PhoneNumber = instructor.PhoneNumber,
                    Email = instructor.Email,
                    LicenceNumber = instructor.InstructorLicence.LicenceNumber,
                    ValidThru = instructor.InstructorLicence.ValidThru
                };

                return View(toEdit);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditInstructor(string id, RegisterInstructorViewModel toEdit, List<int> selectedCategories)
        {
            if (ModelState.IsValid)
            {
                if (!(_context.Users.Where(x => x.Email == toEdit.Email)).Any(y => y.Id != id))
                {
                    await _userService.UpdateInstructorAsync(id, toEdit, selectedCategories);
                    return RedirectToAction(nameof(Instructors));
                }
                ModelState.AddModelError(string.Empty, "Istnieje już użytkownik o takim adresie email.");
            }
            return View(toEdit);
        }

        public async Task<IActionResult> DeleteInstructor(string id)
        {
            var student = await _userService.GetInstructorByIdAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        [HttpPost, ActionName("DeleteInstructor")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteInstructorConfirmed(string id)
        {
            await _userService.RemoveInstructorAsync(id);
            return RedirectToAction(nameof(Instructors));
        }

        public async Task<IActionResult> InstructorDetails(string id)
        {
            var instructor = await _userService.GetInstructorByIdAsync(id);
            var categories = await _context.Categories.ToListAsync();
            var userLicence = await _context.LicenceCategories.Where(x => x.InstructorLicenceId == instructor.InstructorLicence.LicenceId).Select(y => y.CategoryId).ToListAsync();

            List<Category> instructorCategories = new List<Category>();

            foreach (var item in categories)
            {
                if (userLicence.Contains(item.CategoryId))
                {
                    instructorCategories.Add(item);
                }
            }

            ViewBag.InstructorCategories = instructorCategories;

            return View(instructor);
        }
    }
}