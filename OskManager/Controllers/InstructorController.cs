﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OskManager.Data;
using OskManager.Models;
using OskManager.Models.DrivingLessonsViewModels;

namespace OskManager.Controllers
{
    [Authorize(Roles = "Admin, Instructor")]
    public class InstructorController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public InstructorController(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        public IActionResult Events()
        {
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();

            var instructorDrivingLessons = _context.DrivingLessons.Where(x => x.Instructor == user && x.BeginningDate.CompareTo(DateTime.Now) == 1).Include(y => y.Student).Include(y => y.Car).ToList();
            return View(instructorDrivingLessons);
        }

        public IActionResult PastEvents()
        {
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();

            var instructorDrivingLessons = _context.DrivingLessons.Where(x => x.Instructor == user && (x.IsEnded == true || x.IsCanceled == true)).Include(y => y.Student).Include(y => y.Car).ToList();
            return View(instructorDrivingLessons);
        }

        public IActionResult Calendar()
        {
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.Id = user.Id;

            return View();
        }

        [HttpGet]
        public JsonResult GetWorkingHours(string id)
        {
            var workingHours = _context.WorkingHours.Where(x => x.Instructor.Id == id).Include(y => y.Car).ToList();
            return new JsonResult(workingHours);
        }


        [HttpGet]
        public JsonResult GetDrivingLessons(string id)
        {
            var drivingLessons = _context.DrivingLessons.Where(x => x.Instructor.Id == id).Include(y => y.Student).Include(y => y.Car).ToList();
            return new JsonResult(drivingLessons);
        }

        public JsonResult EditEvent([FromBody]EndDrivingLessonViewModel drivingLesson)
        {
            var status = false;
            var v = _context.DrivingLessons.Where(x => x.DrivingLessonId == drivingLesson.EventId).FirstOrDefault();

            if (v != null)
            {
                if (drivingLesson.Status == "canceled")
                {
                    v.IsCanceled = true;
                }
                else if (drivingLesson.Status == "ended")
                {
                    v.IsEnded = true;
                }
                v.Notes = drivingLesson.Notes;
                status = true;
            }

            _context.SaveChanges();

            return new JsonResult(new { status });
        }
    }
}