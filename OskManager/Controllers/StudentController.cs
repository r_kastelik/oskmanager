﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OskManager.Data;
using OskManager.Models;

namespace OskManager.Controllers
{
    [Authorize(Roles = "Student")]
    public class StudentController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public StudentController(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Events()
        {
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();

            var studentDrivingLessons = _context.DrivingLessons.Where(x => x.Student == user && x.BeginningDate.CompareTo(DateTime.Now) == 1).Include(y => y.Instructor).Include(y => y.Car).ToList();
            return View(studentDrivingLessons);
        }

        public IActionResult PastEvents()
        {
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();

            var studentDrivingLessons = _context.DrivingLessons.Where(x => x.Student == user && x.BeginningDate.CompareTo(DateTime.Now) == 1 && x.IsEnded == true).Include(y => y.Instructor).Include(y => y.Car).ToList();
            return View(studentDrivingLessons);
        }

        public IActionResult StudentLessonsHistory(string id)
        {
            var student = _context.Students.Where(x => x.Id == id).FirstOrDefault();
            var userLessons = _context.DrivingLessons.Where(x => x.Student.Id == id && (x.IsCanceled == true || x.IsEnded == true)).Include(x => x.Instructor).Include(x => x.Car).ToList();

            ViewBag.Student = student.FirstName + " " + student.LastName;

            return View(userLessons);
        }

    }
}