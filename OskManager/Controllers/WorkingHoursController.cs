﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OskManager.Data;
using OskManager.Models;
using OskManager.Models.WorkingHoursViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using OskManager.Validation;
using System.Net;

namespace OskManager.Controllers
{
    [Authorize(Roles = "Admin")]
    public class WorkingHoursController : Controller
    {
        private readonly ApplicationDbContext _context;

        public WorkingHoursController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult ManageSettings()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ManageSettings(ManageSettingsViewModel settingsVM)
        {
            if (ModelState.IsValid)
            {
                ReservationSettings settings = new ReservationSettings()
                {
                    MinTime = settingsVM.MinHours * 60 + settingsVM.MinMinutes,
                    MaxTime = settingsVM.MaxHours * 60 + settingsVM.MaxMinutes,
                    ReservationsPerDay = settingsVM.ReservationsPerDay,
                };

                _context.ReservationSettings.Add(settings);
                _context.SaveChanges();
                return RedirectToAction("Instructors", "Users");
            }
            return View(settingsVM);
        }


        public IActionResult Index(string id)
        {
            var currentSettings = _context.ReservationSettings.FirstOrDefault();

            if (currentSettings == null)
            {
                return View("ManageSettings");
            }

            ViewBag.Cars = (from x in _context.Cars
                            select x).ToList();

            ViewBag.Instructor = _context.Instructors.FirstOrDefault(x => x.Id == id);

            ViewBag.Settings = _context.ReservationSettings.FirstOrDefault();

            TimeSpan timeSpan = TimeSpan.FromMinutes(currentSettings.MinTime);
            ViewBag.Snap = timeSpan.ToString();

            return View();
        }

        public JsonResult GetEvents(string id)
        {
            var workingHours = _context.WorkingHours.Where(x => x.Instructor.Id == id).Include(y => y.Car).ToList();

            return new JsonResult(workingHours);
        }

        [HttpPost]
        public JsonResult SaveEvent([FromBody]AddEditWorkingHoursViewModel newwh)
        {
            var status = false;

            var wh = new WorkingHours
            {
                BeginningTime = newwh.BeginningTime,
                EndTime = newwh.EndTime,
                CreatedAt = DateTime.Now,
                Instructor = _context.Instructors.FirstOrDefault(x => x.Id == newwh.InstructorId),
                Car = _context.Cars.FirstOrDefault(x => x.CarId == newwh.CarId)
            };

            if (!(WorkingHoursValidation.ValidateLicence(wh, _context)))
            {
                return new JsonResult(new { status = false, message = "Wystąpił błąd," +
                    " sprawdź ważność licencji instruktora." });
            }
            else
            {
                if (newwh != null)
                {
                    _context.WorkingHours.Add(wh);
                    _context.SaveChanges();
                    status = true;

                }
            }
            return new JsonResult(new { status });
        }

        [HttpPost]
        public JsonResult DeleteEvent([FromBody]AddEditWorkingHoursViewModel toDelete)
        {
            var status = false;

            var v = _context.WorkingHours.FirstOrDefault(x => x.WorkingHoursId == toDelete.EventId);

            if (v != null)
            {
                _context.WorkingHours.Remove(v);
                _context.SaveChanges();
                status = true;
            }

            return new JsonResult(new { status = status });
        }

        public JsonResult EditEvent([FromBody]WorkingHours wh)
        {
            var status = false;

            //Update the event
            var v = _context.WorkingHours.Where(x => x.WorkingHoursId == wh.WorkingHoursId).FirstOrDefault();
            if (v != null)
            {
                v.BeginningTime = wh.BeginningTime;
                v.EndTime = wh.EndTime;
            }

            _context.SaveChanges();
            status = true;


            return new JsonResult(new { status = status });
        }

        [HttpGet]
        public JsonResult GetCarAvailability(int id)
        {
            var workingHours = _context.WorkingHours.Where(x => x.Car.CarId == id).Include(y => y.Car).ToList();

            return new JsonResult(workingHours);
        }
    }
}