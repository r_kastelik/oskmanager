﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OskManager.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using OskManager.Data;
using Microsoft.EntityFrameworkCore;

namespace OskManager.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        //[Authorize]
        public IActionResult Index()
        {
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            if (_userManager.IsInRoleAsync(user, "Admin").Result == true)
            {
                var drivingLessons = _context.DrivingLessons.Where(x=>x.BeginningDate.CompareTo(DateTime.Now) == 1).Include(y => y.Student).Include(y => y.Car).ToList();
                return View(drivingLessons);
            }
            if (_userManager.IsInRoleAsync(user, "Instructor").Result == true)
            {
                var instructorDrivingLessons = _context.DrivingLessons.Where(x => x.Instructor == user && x.BeginningDate.CompareTo(DateTime.Now) == 1).Include(y => y.Student).Include(y => y.Car).ToList();
                return View(instructorDrivingLessons);
            }
            if (_userManager.IsInRoleAsync(user, "Student").Result == true)
            {
                var studentDrivingLessons = _context.DrivingLessons.Where(x => x.Student == user && x.BeginningDate.CompareTo(DateTime.Now)==1).Include(y => y.Instructor).Include(y => y.Car).ToList();
                return View(studentDrivingLessons);
            }

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
