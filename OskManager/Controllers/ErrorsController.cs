﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OskManager.Controllers
{
    public class ErrorsController : Controller
    {
        public IActionResult Index(string message)
        {
            ViewBag.Message = message;

            return View();
        }
    }
}