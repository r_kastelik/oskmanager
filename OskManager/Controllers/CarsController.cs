﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OskManager.Data;
using OskManager.Models;
using OskManager.BLL;
using OskManager.Models.CarsViewModels;
using Microsoft.AspNetCore.Authorization;

namespace OskManager.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CarsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ICarsService _carsService;

        public CarsController(ApplicationDbContext context)
        {
            _context = context;
            _carsService = new CarsService(_context);
        }

        public async Task<IActionResult> Index()
        {
            return View(await _carsService.GetAllAsync());
        }

        public async Task<IActionResult> Details(int id)
        {
            var car = await _carsService.GetByIdAsync(id);

            if (car == null)
            {
                return NotFound();
            }

            return View(car);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Brand,Model,Year,RegistrationNumber")] AddEditCarViewModel car)
        {
            if (ModelState.IsValid)
            {
                Car newCar = new Car()
                {
                    Brand = car.Brand,
                    Model = car.Model,
                    Year = car.Year,
                    RegistrationNumber = car.RegistrationNumber,
                };

                await _carsService.CreateAsync(newCar);
                return RedirectToAction(nameof(Index));
            }
            return View(car);
        }

        // GET: Cars/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var car = await _carsService.GetByIdAsync(id);
            if (car == null)
            {
                return NotFound();
            }
            else
            {
                AddEditCarViewModel toEdit = new AddEditCarViewModel()
                {
                    Brand = car.Brand,
                    Model = car.Model,
                    Year = car.Year,
                    RegistrationNumber = car.RegistrationNumber,
                };

                return View(toEdit);

            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CarId,Brand,Model,Year,RegistrationNumber")] AddEditCarViewModel toEdit)
        {
            if (ModelState.IsValid)
            {
                Car modifiedCar = new Car()
                {
                    Brand = toEdit.Brand,
                    Model = toEdit.Model,
                    Year = toEdit.Year,
                    RegistrationNumber = toEdit.RegistrationNumber,
                };

                await _carsService.UpdateAsync(id, modifiedCar);
                return RedirectToAction(nameof(Index));
            }
            return View(toEdit);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var car = await _carsService.GetByIdAsync(id);
            if (car == null)
            {
                return NotFound();
            }

            return View(car);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _carsService.RemoveAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
