﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OskManager.BLL;
using OskManager.Data;
using OskManager.Models.CourseViewModels;
using OskManager.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace OskManager.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CoursesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ICourseService _courseService;

        public CoursesController(ApplicationDbContext context)
        {
            _context = context;
            _courseService = new CourseService(_context);
        }

        public async Task<IActionResult> Index()
        {
            return View(await _courseService.GetAllCoursesAsync());
        }

        public IActionResult Create()
        {
            ViewBag.Instructors = (from x in _context.Instructors
                                   select new { Id = x.Id, Display = x.FirstName + " " + x.LastName + " (" + x.UserName + ")" }).ToList(); ;

            ViewBag.Categories = (from x in _context.Categories
                                  select x).ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddCourseViewModel course)
        {
            if (ModelState.IsValid)
            {
                Course newCourse = new Course()
                {
                    CourseName = course.CourseName,
                    CourseDescription = course.CourseDescription,
                    BeginningDate = DateTime.Now,
                    CoursePrice = course.Price,
                    LessonHours = course.LessonHours * 60,
                    //Instructor = _context.Instructors.FirstOrDefault(x=>x.Id == course.Instructor)
                };

                await _courseService.CreateAsync(newCourse);
                return RedirectToAction(nameof(Index));
            }
            return View(course);
        }

        public async Task<IActionResult> Details(int id, string keyword)
        {
            var course = await _courseService.GetByIdAsync(id);
            if (course == null)
            {
                return NotFound();
            }

            var studentCourses = _context.StudentCourses.ToList();
            var students = _context.Students.ToList();
            var studentsInCourse = new List<Student>();
            var studentsWithoutCourse = new List<Student>();
            CourseDetailsViewModel courseDetails = new CourseDetailsViewModel();
            courseDetails.Course = course;

            if (!String.IsNullOrEmpty(keyword))
            {
                students = _context.Students.Where(x => x.FirstName.Contains(keyword) || x.LastName.Contains(keyword) || x.Pkk.Contains(keyword)).ToList();
            }


            if (studentCourses.Count > 0)
            {
                foreach (var item in students)
                {
                    if ((studentCourses.Any(x => x.StudentId == item.Id && x.CourseId == id)))
                    {
                        studentsInCourse.Add(item);
                    }
                    else
                    if (!(studentCourses.Any(x => x.StudentId == item.Id)))
                    {
                        studentsWithoutCourse.Add(item);
                    }

                    courseDetails.ClientsInCourse = studentsInCourse;
                    courseDetails.ClientsWithoutCourse = studentsWithoutCourse;
                }
            }
            else
            {
                courseDetails.ClientsInCourse = studentsInCourse;
                courseDetails.ClientsWithoutCourse = students;
            }

            return View(courseDetails);
        }

        public IActionResult AddClient(int id, string keyword)
        {
            ViewBag.Id = id;

            var studentCourses = _context.StudentCourses.ToList();
            var students = _context.Students.ToList();
            var studentsWithoutCourse = new List<Student>();

            if (!String.IsNullOrEmpty(keyword))
            {
                students = _context.Students.Where(x => x.FirstName.Contains(keyword) || x.LastName.Contains(keyword) || x.Pkk.Contains(keyword)).ToList();
            }

            if (studentCourses.Count > 0)
            {
                foreach (var item in students)
                {
                    if (!(studentCourses.Any(x => x.StudentId == item.Id)))
                    {
                        studentsWithoutCourse.Add(item);
                    }
                }
            }
            else
                return View(students);

            return View(studentsWithoutCourse);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var course = await _courseService.GetByIdAsync(id);
            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            //await _courseService.RemoveAsync(id);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public JsonResult RemoveUserFromCourse(string id)
        {
            var status = false;
            var v = _context.Students.FirstOrDefault(x => x.Id == id);

            if (v != null)
            {
                var course = _context.StudentCourses.FirstOrDefault(x => x.StudentId == id);
                _context.Remove(course);
                v.AvailableMinutes = 0;

                _context.SaveChanges();
            }

            return new JsonResult(new { status });
        }


        [HttpPost]
        public JsonResult AddUserToCourse([FromBody]AddUserViewModel ids)
        {
            var status = false;

            var student = _context.Students.Where(x => x.Id == ids.StudentId).FirstOrDefault();
            var course = _context.Courses.FirstOrDefault(x => x.CourseId == ids.CourseId);

            student.AvailableMinutes += course.LessonHours;

            Debt debt = new Debt() { Amount = course.CoursePrice, Title = "Opłata za kurs: " 
                + course.CourseDescription, Student = student, CreatedAt = DateTime.Now };
            student.Debts.Add(debt);

            StudentCourse sc = new StudentCourse() { CourseId = ids.CourseId, StudentId = ids.StudentId };
            _context.StudentCourses.Add(sc);
            _context.SaveChanges();

            return new JsonResult(new { status });
        }

        [HttpPost]
        public JsonResult RenewUserCourse([FromBody]AddUserViewModel ids)
        {
            var status = false;

            var student = _context.Students.Where(x => x.Id == ids.StudentId).FirstOrDefault();
            var course = _context.Courses.FirstOrDefault(x => x.CourseId == ids.CourseId);

            student.AvailableMinutes += course.LessonHours;

            Debt debt = new Debt() { Amount = course.CoursePrice, Title = "Odnowienie kursu: " + course.CourseDescription, Student = student, CreatedAt = DateTime.Now };
            student.Debts.Add(debt);

            _context.SaveChanges();

            return new JsonResult(new { status });
        }

        public ActionResult _ClientInCourse()
        {
            var model = _context.Students.ToList();
                return PartialView("_ClientInCourse");
        }
    }
}