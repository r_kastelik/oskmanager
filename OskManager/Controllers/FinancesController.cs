﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OskManager.Data;
using OskManager.Models;
using OskManager.Models.FinanceViewModels;

namespace OskManager.Controllers
{
    [Authorize]
    public class FinancesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FinancesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult All()
        {
            var debts =_context.Debts.Include(x=>x.Student).ToList();
            var payments = _context.Payments.Include(x => x.Student).ToList();

            DebtsAndPaymentsViewModel dp = new DebtsAndPaymentsViewModel() { Debts = debts, Payments = payments };

            return View(dp);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Payments()
        {
            var payments = _context.Payments.Include(x => x.Student).ToList();
            double sum = payments.Sum(t => t.Amount);
            ViewBag.Sum = sum;

            return View(payments);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Debts()
        {
            var debts = _context.Debts.Include(x => x.Student).ToList();
            double sum = debts.Sum(t => t.Amount);

            ViewBag.Sum = sum;

            return View(debts);
        }

        [Authorize(Roles = "Student")]
        public IActionResult UserFinances(string id)
        {
            var user = _context.Students.Where(x => x.Id == id).FirstOrDefault();
            var debts = _context.Debts.Include(x => x.Student).ToList();
            var payments = _context.Payments.Include(x => x.Student).ToList();

            UserFinancesViewModel dp = new UserFinancesViewModel() { Debts = debts, Payments = payments, User = user };

            double paymentsSum = payments.Sum(t => t.Amount);
            double debtsSum = debts.Sum(t => t.Amount);

            ViewBag.Payments = paymentsSum;
            ViewBag.Debts = debtsSum;
            ViewBag.Sum = paymentsSum - debtsSum;


            return View(dp);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public JsonResult AddPayment([FromBody]AddPaymentViewModel payment)
        {
            var status = false;

            if ((payment.Amount != 0) && (payment.Title != null))
            {
                var student = _context.Students.Where(x => x.Id == payment.StudentId).FirstOrDefault();

                Payment newPayment = new Payment();
                newPayment.Title = payment.Title;
                newPayment.Amount = payment.Amount;
                newPayment.CreatedAt = DateTime.Now;
                newPayment.Student = student;

                _context.Payments.Add(newPayment);
                _context.SaveChanges();
            }

            return new JsonResult(new { status });
        }

    }
}