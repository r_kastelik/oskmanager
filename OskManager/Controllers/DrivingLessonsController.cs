﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OskManager.Data;
using Microsoft.EntityFrameworkCore;
using OskManager.Models;
using OskManager.Models.DrivingLessonsViewModels;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using OskManager.Validation;
using System.Net;

namespace OskManager.Controllers
{
    [Authorize(Roles = "Student, Admin")]
    public class DrivingLessonsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DrivingLessonsController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index(string id)
        {
            var student = _context.Students.Where(x => x.Id == id).FirstOrDefault();
            var currentSettings = _context.ReservationSettings.FirstOrDefault();

            if (currentSettings != null)
            {
                ViewBag.Instructors = (from x in _context.Instructors
                                       select x).ToList();

                ViewBag.Settings = currentSettings;

                TimeSpan timeSpan = TimeSpan.FromMinutes(currentSettings.MinTime);
                ViewBag.Snap = timeSpan.ToString();

                ViewBag.AvailableHours = student.AvailableMinutes;

                return View();
            }

            else
                return RedirectToAction("Index", "Errors",
                    new { message = "Rezerwacje nie zostały skonfigurowane przez administratora." });
        }

        public JsonResult GetEvents(string id)
        {
            var drivingLessons = _context.DrivingLessons.Where(x => x.Student.Id == id).Include(y => y.Instructor).ToList();

            return new JsonResult(drivingLessons);
        }

        [HttpGet]
        public JsonResult GetNotAvailableEvents(string id)
        {
            var drivingLessons = _context.DrivingLessons.Where(y => y.Instructor.Id == id).Include(y => y.Instructor).Include(x => x.Student).ToList();

            return new JsonResult(drivingLessons);
        }


        [HttpPost]
        public JsonResult SaveEvent([FromBody]AddEditDrivingLessonsViewModel lessonEvent)
        {
            var lesson = new DrivingLesson
            {
                BeginningDate = lessonEvent.BeginningTime,
                EndingDate = lessonEvent.EndTime,
                CreatedAt = DateTime.Now
            };

            TimeSpan varTime = lesson.EndingDate - lesson.BeginningDate;
            double fractionalMinutes = varTime.TotalMinutes;
            int wholeMinutes = (int)fractionalMinutes;
            lesson.LessonTime = wholeMinutes;

            var student = _context.Students.FirstOrDefault(x => x.Id == lessonEvent.StudentId);
            lesson.Instructor = _context.Instructors.FirstOrDefault(x => x.Id == lessonEvent.InstructorId);
            lesson.Student = student;
            var carWh = _context.WorkingHours.Where(x => x.BeginningTime <= lesson.BeginningDate && x.EndTime >= lesson.EndingDate).Include(y => y.Car).FirstOrDefault();
            lesson.Car = carWh.Car;


            if (DrivingLessonValidation.IsOverlapping(lesson, _context))
            {
                return new JsonResult(new { status = false, message = "Niestety, wybrany termin jest już zajęty." });
            }
            else if (!(DrivingLessonValidation.MatchesSettings(lesson, _context)))
            {
                return new JsonResult(new { status = false, message = "Wystąpił błąd rezerwacji." });
            }
            else if (!(DrivingLessonValidation.HasEnoughAvailableTime(lesson.Student, wholeMinutes)))
            {
                return new JsonResult(new { status = false, message = "Brak dostępnych minut." });

            }
            if (lessonEvent != null)
            {
                _context.DrivingLessons.Add(lesson);
                _context.SaveChanges();
                student.AvailableMinutes -= wholeMinutes;
                _context.SaveChanges();

            }
            return new JsonResult(new { status = true });
        }

        [HttpPost]
        public JsonResult DeleteEvent([FromBody]AddEditDrivingLessonsViewModel toDelete)
        {
            var status = false;

            var student = _context.Students.FirstOrDefault(x => x.Id == toDelete.StudentId);
            var v = _context.DrivingLessons.FirstOrDefault(x => x.DrivingLessonId == toDelete.EventId);
            var minutes = DrivingLessonValidation.GetMinutes(v.BeginningDate, v.EndingDate);

            if (v != null)
            {
                _context.DrivingLessons.Remove(v);
                student.AvailableMinutes += minutes;
                _context.SaveChanges();
                status = true;
            }

            return new JsonResult(new { status });
        }

        public JsonResult EditEvent([FromBody]AddEditDrivingLessonsViewModel wh)
        {
            var status = false;

            var student = _context.Students.FirstOrDefault(x => x.Id == wh.StudentId);
            var v = _context.DrivingLessons.Where(x => x.DrivingLessonId == wh.EventId).FirstOrDefault();

            int timeAfter = DrivingLessonValidation.GetMinutes(wh.BeginningTime, wh.EndTime);
            int timeBefore = DrivingLessonValidation.GetMinutes(v.BeginningDate, v.EndingDate);
            int timeDifference = timeBefore - timeAfter;

            if (timeAfter > timeBefore)
            {
                if (!(DrivingLessonValidation.HasEnoughAvailableTime(student, timeBefore - timeAfter)))
                {
                    return new JsonResult(new { status = false, message = "Brak dostępnych minut." });
                }
            }

            if (v != null)
            {
                v.BeginningDate = wh.BeginningTime;
                v.EndingDate = wh.EndTime;
                v.LessonTime = timeAfter;

                student.AvailableMinutes += timeDifference;
            }

            _context.SaveChanges();
            status = true;

            return new JsonResult(new { status });
        }

        [HttpGet]
        public JsonResult GetInstructorAvailability(string id)
        {
            double validHours = 24;
            var latest = DateTime.Now.AddHours(-validHours);

            var workingHours = _context.WorkingHours.Where(x => x.Instructor.Id == id && x.Car != null && x.CreatedAt <= latest).Include(y => y.Car).ToList();
            return new JsonResult(workingHours);
        }

    }
}