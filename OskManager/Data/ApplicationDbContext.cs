﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OskManager.Models;
using Microsoft.AspNetCore.Identity;

namespace OskManager.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<StudentCourse>()
                .HasKey(t => new { t.StudentId, t.CourseId });

            builder.Entity<LicenceCategory>()
                    .HasKey(t => new { t.CategoryId, t.InstructorLicenceId });

            builder.Entity<ApplicationUser>(entity => { entity.ToTable(name: "User"); });

            builder.Entity<ApplicationUser>(b =>
            {
                b.Property(u => u.Id).HasColumnName("UserId");
            });

            builder.Entity<IdentityRole>(entity => { entity.ToTable(name: "Role"); });
            builder.Entity<IdentityUserRole<string>>(entity => { entity.ToTable("UserRoles"); });
            builder.Entity<IdentityUserClaim<string>>(entity => { entity.ToTable("UserClaims"); });
            builder.Entity<IdentityUserLogin<string>>(entity => { entity.ToTable("UserLogins"); });
            builder.Entity<IdentityUserToken<string>>(entity => { entity.ToTable("UserToken"); });
            builder.Entity<IdentityRoleClaim<string>>(entity => { entity.ToTable("RoleClaim"); });
        }


        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<DrivingLesson> DrivingLessons { get; set; }
        public DbSet<InstructorLicence> InstructorLicences { get; set; }
        public DbSet<Lecture> Lectures { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<LicenceCategory> LicenceCategories { get; set; }
        public DbSet<StudentCourse> StudentCourses { get; set; }
        public DbSet<WorkingHours> WorkingHours { get; set; }
        public DbSet<ReservationSettings> ReservationSettings { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Debt> Debts { get; set; }
    }
}
