﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using OskManager.Data;
using OskManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Configuration
{
    public static class DbInitializer
    {
        public static void Seed(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            context.Database.EnsureCreated();

            string[] categories = { "A", "B", "C", "D", "B+E", "C+E", "T" };


            if (!context.Categories.Any())
            {
                foreach(var category in categories)
                {
                    context.Categories.Add(new Category() { CategoryName = category });
                    context.SaveChanges();
                }
            }

        }
    }
}
