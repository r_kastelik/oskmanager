﻿using OskManager.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class Student : ApplicationUser
    {
        public string Pkk { get; set; }

        public virtual ICollection<DrivingLesson> StudentDrivingLessons { get; set; }
        public virtual ICollection<StudentCourse> StudentCourses { get; set; }
        public virtual ICollection<Debt> Debts { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }

        public int AvailableMinutes { get; set; }

        public Student()
        {
            StudentDrivingLessons = new HashSet<DrivingLesson>();
            StudentCourses = new HashSet<StudentCourse>();
            Debts = new HashSet<Debt>();
            Payments = new HashSet<Payment>();
        }

    }
}
