﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class Course
    {
        [Key]
        public int CourseId { get; set; }

        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public string CategoryName { get; set; }
        public int CoursePrice { get; set; }
        public int LessonHours { get; set; }

        public DateTime BeginningDate { get; set; }
        public DateTime EndingDate { get; set; }
        public bool IsEnded { get; set; }
        public virtual ICollection<StudentCourse> StudentCourses { get; set; }

        public Course()
        {
            StudentCourses = new HashSet<StudentCourse>();
        }
    }
}
