﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class Instructor : ApplicationUser
    {
        public virtual InstructorLicence InstructorLicence { get; set; }

        public virtual ICollection<DrivingLesson> InstructorDrivingLessons { get; set; }

        public virtual ICollection<WorkingHours> WorkingHours { get; set; }

        public Instructor()
        {
            InstructorDrivingLessons = new HashSet<DrivingLesson>();
            WorkingHours = new HashSet<WorkingHours>();
        }

    }
}
