﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class InstructorLicence
    {
        [Key]
        public int LicenceId { get; set; }
        public string LicenceNumber { get; set; }
        public DateTime ValidThru { get; set; }

        public virtual ICollection<LicenceCategory> Categories { get; set; }

        public InstructorLicence()
        {
            Categories = new HashSet<LicenceCategory>();
        }
    }
}
