﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class Lecture
    {
        [Key]
        public int LectureId { get; set; }

        public DateTime BeginningDate { get; set; }
        public DateTime EndingDate { get; set; }
        public float LessonTime { get; set; }

        public virtual Instructor Instructor { get; set; }

        public virtual Course Course { get; set; }
    }
}
