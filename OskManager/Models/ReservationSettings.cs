﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class ReservationSettings
    {
        public int Id { get; set; }
        public float MinTime { get; set; }
        public float MaxTime { get; set; }
        public int ReservationsPerDay { get; set; }
    }
}
