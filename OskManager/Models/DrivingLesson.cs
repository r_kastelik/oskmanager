﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class DrivingLesson
    {
        [Key]
        public int DrivingLessonId { get; set; }

        public DateTime BeginningDate { get; set; }
        public DateTime EndingDate { get; set; }
        public DateTime CreatedAt { get; set; }

        public float LessonTime { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsEnded { get; set; }
        public string Notes { get; set; }

        public DateTime ConfirmDate { get; set; }

        public virtual Instructor Instructor { get; set; }
        public virtual Student Student { get; set; }
        public virtual Car Car { get; set; }


    }
}
