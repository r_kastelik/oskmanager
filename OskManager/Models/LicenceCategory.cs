﻿using System;

namespace OskManager.Models
{
    public class LicenceCategory
    {
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public int InstructorLicenceId { get; set; }
        public virtual InstructorLicence InstructorLicence { get; set; }
    }
}