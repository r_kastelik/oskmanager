﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OskManager.Models
{
    public class Address
    {
        public int AddressId { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }

        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}