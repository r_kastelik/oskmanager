﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class WorkingHours
    {
        [Key]
        public int WorkingHoursId { get; set; }

        public DateTime BeginningTime { get; set; }
        public DateTime EndTime { get; set; }

        public DateTime CreatedAt { get; set; }

        public virtual Car Car { get; set; }
        public virtual Instructor Instructor { get; set; }

        internal object Include(Func<object, object> p)
        {
            throw new NotImplementedException();
        }
    }
}
