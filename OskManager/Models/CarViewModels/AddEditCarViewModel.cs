﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.CarsViewModels
{
    public class AddEditCarViewModel
    {
        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Marka")]
        public string Brand { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Model")]
        public string Model { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Rok produkcji")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Numer rejestracjyjny")]
        public string RegistrationNumber { get; set; }
        
    }
}
