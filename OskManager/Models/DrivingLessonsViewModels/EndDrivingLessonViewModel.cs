﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.DrivingLessonsViewModels
{
    public class EndDrivingLessonViewModel
    {
        public int EventId { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }

    }
}
