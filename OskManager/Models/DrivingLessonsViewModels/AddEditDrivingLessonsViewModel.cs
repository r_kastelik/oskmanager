﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.DrivingLessonsViewModels
{
    public class AddEditDrivingLessonsViewModel
    {
        public int EventId { get; set; }
        public string StudentId { get; set; }
        public string InstructorId { get; set; }

        public DateTime BeginningTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
