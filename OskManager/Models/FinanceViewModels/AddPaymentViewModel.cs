﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.FinanceViewModels
{
    public class AddPaymentViewModel
    {
        public string Title { get; set; }
        public float Amount { get; set; }

        public string StudentId { get; set; }
    }
}
