﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.FinanceViewModels
{
    public class UserFinancesViewModel
    {
        public List<Debt> Debts { get; set; }
        public List<Payment> Payments { get; set; }
        public Student User { get; set; }

    }
}
