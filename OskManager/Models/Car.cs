﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class Car
    {
        [Key]
        public int CarId { get; set; }

        public string Brand { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string RegistrationNumber { get; set; }
        
        public virtual ICollection<DrivingLesson> DrivingLessons { get; set; }
        public virtual ICollection<WorkingHours> WorkingHours { get; set; }


        public Car()
        {
            DrivingLessons = new HashSet<DrivingLesson>();
            WorkingHours = new HashSet<WorkingHours>();

        }

    }
}
