﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.CourseViewModels
{
    public class AddCourseViewModel
    {
        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Nazwa kursu")]
        public string CourseName { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Opis kursu")]
        public string CourseDescription { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Kwota za kurs")]
        [Range(1, 100000)]
        public int Price { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Godziny zajęć")]
        [Range(1, 60)]
        public int LessonHours { get; set; }
    }
}
