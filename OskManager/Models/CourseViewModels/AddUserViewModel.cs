﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.CourseViewModels
{
    public class AddUserViewModel
    {
        public int CourseId { get; set; }
        public string StudentId { get; set; }
    }
}
