﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.CourseViewModels
{
    public class AddUsersToCourseViewModel
    {
        public List<Student> Users { get; set; }

        public List<Student> UsersInCourse { get; set; }
    }
}
