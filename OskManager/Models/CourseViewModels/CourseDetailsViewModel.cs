﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.CourseViewModels
{
    public class CourseDetailsViewModel
    {
        public Course Course { get; set; }
        public List<Student> ClientsInCourse { get; set; }
        public List<Student> ClientsWithoutCourse { get; set; }

    }
}
