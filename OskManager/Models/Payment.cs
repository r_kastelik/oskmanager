﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models
{
    public class Payment
    {
        public int PaymentId { get; set; }
        public string Title { get; set; }
        public float Amount { get; set; }

        public Student Student { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
