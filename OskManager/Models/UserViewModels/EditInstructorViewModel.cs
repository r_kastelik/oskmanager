﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.UserViewModels
{
    public class EditInstructorViewModel
    {
        public int CategoryId { get; set; }
        public int CategoryName { get; set; }
    }
}
