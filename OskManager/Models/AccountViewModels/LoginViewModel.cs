﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required (ErrorMessage = "Pole '{0}' jest wymagane")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Zapamiętaj mnie")]
        public bool RememberMe { get; set; }
    }
}
