﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.AccountViewModels
{
    public class RegisterInstructorViewModel
    {
        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Pesel")]
        public string Pesel { get; set; }


        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }


        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Telefon")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Pole '{0}' jest wymagane")]
        [Display(Name = "Numer licencji")]
        public string LicenceNumber { get; set; }

        [Required(ErrorMessage = "Podaj datę ważności licencji")]
        [Display(Name = "Data ważności licencji")]
        public DateTime ValidThru { get; set; }

    }
}
