﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.WorkingHoursViewModels
{
    public class AddEditWorkingHoursViewModel
    {
        public string InstructorId { get; set; }
        public int EventId { get; set; }
        public int CarId { get; set; }

        public DateTime BeginningTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
