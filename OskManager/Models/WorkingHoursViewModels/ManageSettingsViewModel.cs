﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.Models.WorkingHoursViewModels
{
    public class ManageSettingsViewModel
    {
        [Display(Name = "Minimalna długość zajęć")]
        public float MinHours { get; set; }
        [Display(Name = "Maksymalna długość zajęć")]
        public float MaxHours { get; set; }

        public float MinMinutes { get; set; }
        public float MaxMinutes { get; set; }

        [Display(Name = "Maksymalna ilość rezerwacji na dzień")]
        public int ReservationsPerDay { get; set; }

    }
}
