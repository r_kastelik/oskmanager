﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OskManager.Models;

namespace OskManager.BLL
{
    public interface ICourseService
    {
        Task CreateAsync(Course course);
        Task<List<Course>> GetAllCoursesAsync();
        Task<Course> GetByIdAsync(int id);
    }
}