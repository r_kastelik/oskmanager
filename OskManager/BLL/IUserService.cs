﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OskManager.Models;
using OskManager.Models.AccountViewModels;

namespace OskManager.BLL
{
    public interface IUserService
    {
        Task<List<Instructor>> GetAllInstructorsAsync();
        Task<List<Student>> GetAllStudentsAsync();
        Task<Instructor> GetInstructorByIdAsync(string id);
        Task<Student> GetStudentByIdAsync(string id);
        Task RemoveInstructorAsync(string id);
        Task RemoveStudentAsync(string id);
        Task UpdateStudentAsync(string id, RegisterStudentViewModel student);
        Task UpdateInstructorAsync(string id, RegisterInstructorViewModel student, List<int> selectedCategories);
    }
}