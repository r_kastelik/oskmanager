﻿using System;
using System.Collections.Generic;
using OskManager.Models;
using System.Threading.Tasks;

namespace OskManager.BLL
{
    public interface ICarsService
    {
        Task CreateAsync(Car car);
        Task<List<Car>> GetAllAsync();
        Task<Car> GetByIdAsync(int id);
        Task RemoveAsync(int id);
        Task UpdateAsync(int id, Car car);
    }
}