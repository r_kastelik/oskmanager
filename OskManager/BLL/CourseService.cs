﻿using Microsoft.EntityFrameworkCore;
using OskManager.Data;
using OskManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.BLL
{
    public class CourseService : ICourseService
    {
        private readonly ApplicationDbContext _context;

        public CourseService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Course>> GetAllCoursesAsync()
        {
            var courses = await _context.Courses.ToListAsync();

            foreach (var item in courses)
            {
                item.LessonHours = item.LessonHours / 60;
            }

            return courses;
        }

        public async Task<Course> GetByIdAsync(int id)
        {
            return await _context.Courses.Where(x=>x.CourseId == id).FirstOrDefaultAsync();
        }


        public async Task CreateAsync(Course course)
        {
            await _context.Courses.AddAsync(course);
            _context.SaveChanges();
        }

        public async Task RemoveAsync(int id)
        {
            var toRemove = await _context.Courses.FirstOrDefaultAsync(x => x.CourseId == id);
            var studentCourses = await _context.StudentCourses.Where(x => x.CourseId == id).ToListAsync();
            var lectures = await _context.Lectures.Where(x => x.LectureId == id).ToListAsync();
        }
    }
}
