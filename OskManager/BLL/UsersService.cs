﻿using Microsoft.EntityFrameworkCore;
using OskManager.Data;
using OskManager.Models;
using OskManager.Models.AccountViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.BLL
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;

        public UserService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Student>> GetAllStudentsAsync()
        {
            return await _context.Students.ToListAsync();
        }

        public async Task<List<Instructor>> GetAllInstructorsAsync()
        {
            return await _context.Instructors.ToListAsync();
        }

        public async Task<Student> GetStudentByIdAsync(string id)
        {
            return await _context.Students.Where(x => x.Id == id).Include(y => y.Address).FirstOrDefaultAsync();
        }

        public async Task<Instructor> GetInstructorByIdAsync(string id)
        {
            return await _context.Instructors.Where(x => x.Id == id).Include(y => y.Address)
                .Include(z => z.InstructorLicence).FirstOrDefaultAsync();
        }

        public async Task RemoveStudentAsync(string id)
        {
            var toRemove = await _context.Students.Where(x => x.Id == id).Include(y => y.Address).FirstOrDefaultAsync();
            _context.Addresses.Remove(toRemove.Address);
            _context.Students.Remove(toRemove);

            _context.SaveChanges();
        }

        public async Task RemoveInstructorAsync(string id)
        {
            var toRemove = await _context.Instructors.Where(x => x.Id == id).Include(y => y.Address)
                .Include(z=>z.InstructorLicence).FirstOrDefaultAsync();

            var licenceCategories = _context.LicenceCategories.Where(x => x.InstructorLicenceId == toRemove.InstructorLicence.LicenceId);

            _context.LicenceCategories.RemoveRange(licenceCategories);
            _context.InstructorLicences.Remove(toRemove.InstructorLicence);
            _context.Addresses.Remove(toRemove.Address);
            _context.Instructors.Remove(toRemove);
           
            _context.SaveChanges();
        }


        public async Task UpdateStudentAsync(string id, RegisterStudentViewModel student)
        {
            var toUpdate = await _context.Students.Where(x => x.Id == id).Include(y=>y.Address).FirstOrDefaultAsync();


            toUpdate.UserName = student.Email;
            toUpdate.FirstName = student.FirstName;
            toUpdate.LastName = student.LastName;
            toUpdate.Pesel = student.Pesel;
            toUpdate.PhoneNumber = student.PhoneNumber;
            toUpdate.Email = student.Email;
            toUpdate.Pkk = student.Pkk;

            toUpdate.Address.Street = student.Street;
            toUpdate.Address.City = student.City;
            toUpdate.Address.PostalCode = student.PostalCode;

            await _context.SaveChangesAsync();

            _context.SaveChanges();
        }

        public async Task UpdateInstructorAsync(string id, RegisterInstructorViewModel instructor, List<int> selectedCategories)
        {
            var toUpdate = await _context.Instructors.Where(x => x.Id == id).Include(y => y.Address)
                .Include(z => z.InstructorLicence).FirstOrDefaultAsync();

            toUpdate.UserName = instructor.Email;
            toUpdate.FirstName = instructor.FirstName;
            toUpdate.LastName = instructor.LastName;
            toUpdate.Pesel = instructor.Pesel;
            toUpdate.PhoneNumber = instructor.PhoneNumber;
            toUpdate.Email = instructor.Email;

            toUpdate.Address.Street = instructor.Street;
            toUpdate.Address.City = instructor.City;
            toUpdate.Address.PostalCode = instructor.PostalCode;

            toUpdate.InstructorLicence.LicenceNumber = instructor.LicenceNumber;
            toUpdate.InstructorLicence.ValidThru = instructor.ValidThru;

            var lc = _context.LicenceCategories.Where(x => x.InstructorLicence == toUpdate.InstructorLicence);
            _context.LicenceCategories.RemoveRange(lc);

            await _context.SaveChangesAsync();

            if (selectedCategories != null)
            {
                foreach (var item in selectedCategories)
                {
                    Category category = await _context.Categories.FindAsync(item);
                    LicenceCategory licenceCategory = new LicenceCategory() { InstructorLicence = toUpdate.InstructorLicence, Category = category };
                        await _context.LicenceCategories.AddAsync(licenceCategory);
                }
            }

            await _context.SaveChangesAsync();

            _context.SaveChanges();
        }
    }
}
