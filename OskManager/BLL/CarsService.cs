﻿using Microsoft.EntityFrameworkCore;
using OskManager.Data;
using OskManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OskManager.BLL
{
    public class CarsService : ICarsService
    {
        private readonly ApplicationDbContext _context;

        public CarsService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Car>> GetAllAsync()
        {
            return await _context.Cars.ToListAsync();
        }

        public async Task<Car> GetByIdAsync(int id)
        {
            return await _context.Cars.SingleOrDefaultAsync(x => x.CarId == id);
        }

        public async Task CreateAsync(Car car)
        {
            await _context.Cars.AddAsync(car);
            _context.SaveChanges();
        }

        public async Task RemoveAsync(int id)
        {
            var toRemove = await _context.Cars.SingleOrDefaultAsync(x => x.CarId == id);
            _context.Cars.Remove(toRemove);

            _context.SaveChanges();
        }

        public async Task UpdateAsync(int id, Car car)
        {
            var toUpdate = await _context.Cars.SingleOrDefaultAsync(x => x.CarId == id);

            toUpdate.Brand = car.Brand;
            toUpdate.Model = car.Model;
            toUpdate.Year = car.Year;
            toUpdate.RegistrationNumber = car.RegistrationNumber;

            _context.SaveChanges();
        }
    }
}
