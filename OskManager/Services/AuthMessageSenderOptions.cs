﻿namespace OskManager.Services
{
    public class AuthMessageSenderOptions
    {
        public string EmailAccount { get; set; }
        public string EmailPassword { get; set; }
    }
}